<terminal>
  <ul>
    <li each={ items } class={output: type=='output'}>
      <div>
        <pre class={prompt: type=='input' }>{text}</pre>
      </div>
    </li>
  </ul>

  <div>
    <form onsubmit={ add }>
      <span class='prompt'></span> <input ref="input" onkeyup={ edit } autofocus>
    </form>
  </div>

  <script>
    this.items = opts.items

    edit(e) {
      this.text = e.target.value
    }

    add(e) {
      self = this
      e.preventDefault()
      switch(self.text) {
        case '':
	  self.items.push({type: 'input', text: self.text})
	  break;
        case 'help':
	  self.items.push({type: 'input', text: self.text})
          for(var command in opts.commands) {
            const desc = opts.commands[command]['desc']
            if(desc){
            self.items.push(
              {
                type: 'output',
                text: command + ': ' + desc
              }
            )
          }
          }
          break;
        case 'clear':
          self.items = []
          break;
        default:
          self.items.push({type: 'input', text: self.text})
          self.items.push({type: 'output', text: self.output(self.text)})
      }
      self.text = self.refs.input.value = ''
    }

    output(input) {
      return(
        opts.commands[self.text] && opts.commands[self.text]['output']
        ||
        'command not found: ' + input
      )
    }
  </script>

  <style>
    * {
      margin: 0;
    }

    .prompt::before {
      content: "~/pablo$> ";
      color: #C0A239;
      font-weight: 700;
    }

    p, .output {
      margin-top: 10px;
      margin-bottom: 10px;
    }

    ul {
      list-style-type: none;
      padding: 0;
    }

    input {
      background-color: rgba(0,0,0,0.4);
      color: white;
      border: 0;
      font: 1.3rem Inconsolata, monospace;
      width: 90%;
    }
  </style>
</terminal>
